﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class NotificationMenu : Form
    {
        DB db;
        string WorkerID;
        Form prev;
        public NotificationMenu(DB db, string WorkerID, Form prev)// Previous form workerid and datebase
        {
            this.db = db;
            this.WorkerID = WorkerID;
            this.prev = prev;
            InitializeComponent();
        }

        private void sendEmail(object sender, EventArgs e)
        {
            List<string> emails = new List<string>();
            try
            {
                if (DstBox.Text == "All")
                    db.Query("Select Email from Workers where WorkerID <> "+WorkerID);
                else
                    db.Query("Select Email from Workers where WorkerPosition='"+DstBox.Text.Substring(0,DstBox.Text.Length-1)+"' and WorkerID <> " + WorkerID);
                for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
                    emails.Add(DB.ds.Tables[0].Rows[i]["Email"].ToString());
                Program.SendEmail(emails,SubjectBox.Text, ContentBox.Text);
                ContentBox.Text = "";
                SubjectBox.Text = "";
                DstBox.Text = "";
            }
            catch
            {
                MessageBox.Show("Please Enter All/Assistant Managers/ Managers/Shift Managers/Ticket Sellers/ Cleaners/ Ticket Checkers");
            }
        }

        private void Back(object sender, EventArgs e)
        {
            this.prev.Show();
            this.Hide();
        }

        private void NotificationMenu_Load(object sender, EventArgs e)
        {

        }
    }
}
