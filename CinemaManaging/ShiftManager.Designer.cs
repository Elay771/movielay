﻿namespace CinemaManaging
{
    partial class ShiftManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.YourSalary = new System.Windows.Forms.Button();
            this.YourShifts = new System.Windows.Forms.Button();
            this.YourData = new System.Windows.Forms.Button();
            this.LogOut = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.ShiftsOfTheWeek = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // YourSalary
            // 
            this.YourSalary.Location = new System.Drawing.Point(12, 104);
            this.YourSalary.Name = "YourSalary";
            this.YourSalary.Size = new System.Drawing.Size(144, 23);
            this.YourSalary.TabIndex = 14;
            this.YourSalary.Text = "Your Salary";
            this.YourSalary.UseVisualStyleBackColor = true;
            this.YourSalary.Click += new System.EventHandler(this.YourSalary_Click);
            // 
            // YourShifts
            // 
            this.YourShifts.Location = new System.Drawing.Point(12, 133);
            this.YourShifts.Name = "YourShifts";
            this.YourShifts.Size = new System.Drawing.Size(144, 23);
            this.YourShifts.TabIndex = 13;
            this.YourShifts.Text = "Your Shifts";
            this.YourShifts.UseVisualStyleBackColor = true;
            this.YourShifts.Click += new System.EventHandler(this.YourShifts_Click);
            // 
            // YourData
            // 
            this.YourData.Location = new System.Drawing.Point(12, 75);
            this.YourData.Name = "YourData";
            this.YourData.Size = new System.Drawing.Size(144, 23);
            this.YourData.TabIndex = 12;
            this.YourData.Text = "Your Data";
            this.YourData.UseVisualStyleBackColor = true;
            this.YourData.Click += new System.EventHandler(this.YourData_Click);
            // 
            // LogOut
            // 
            this.LogOut.Location = new System.Drawing.Point(701, 398);
            this.LogOut.Name = "LogOut";
            this.LogOut.Size = new System.Drawing.Size(87, 40);
            this.LogOut.TabIndex = 18;
            this.LogOut.Text = "Log Out";
            this.LogOut.UseVisualStyleBackColor = true;
            this.LogOut.Click += new System.EventHandler(this.LogOut_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(223, 31);
            this.label1.TabIndex = 21;
            this.label1.Text = "Hi, Shift Manager";
            // 
            // ShiftsOfTheWeek
            // 
            this.ShiftsOfTheWeek.Location = new System.Drawing.Point(12, 162);
            this.ShiftsOfTheWeek.Name = "ShiftsOfTheWeek";
            this.ShiftsOfTheWeek.Size = new System.Drawing.Size(143, 23);
            this.ShiftsOfTheWeek.TabIndex = 25;
            this.ShiftsOfTheWeek.Text = "Manage Shifts";
            this.ShiftsOfTheWeek.UseVisualStyleBackColor = true;
            this.ShiftsOfTheWeek.Click += new System.EventHandler(this.ShiftsOfTheWeek_Click_1);
            // 
            // ShiftManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.ShiftsOfTheWeek);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LogOut);
            this.Controls.Add(this.YourSalary);
            this.Controls.Add(this.YourShifts);
            this.Controls.Add(this.YourData);
            this.Name = "ShiftManager";
            this.Load += new System.EventHandler(this.ShiftManager_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button YourSalary;
        private System.Windows.Forms.Button YourShifts;
        private System.Windows.Forms.Button YourData;
        private System.Windows.Forms.Button LogOut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ShiftsOfTheWeek;
    }
}