﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class LeastHelpfulWorker : Form
    {
        DB db;
        DateTime from, to;
        Form sender;
        string WorkerID;
        public LeastHelpfulWorker(DB db, DateTime from, DateTime to, Form sender, string WorkerID)
        {
            this.sender = sender;
            this.WorkerID = WorkerID;
            this.from = from;
            this.to = to;
            this.db = db;
            InitializeComponent();
        }

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            this.sender.Show();
        }

        private void LeastHelpfulWorker_Load(object sender, EventArgs e)
        {
            int min = -1;
            bool first = true;
            string minID = "";
            db.Query("Select * from Workers");
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {
                string currWorkerID = DB.ds.Tables[0].Rows[i]["WorkerID"].ToString();
                db.Query("Select Count(*) from TheaterMovies RIGHT JOIN MovieOrders  ON TheaterMovies.TheaterMovieID = MovieOrders.TheaterMovieID  where MovieOrders.WorkerID = "+currWorkerID+" and TheaterMovies.StartTime between #"+from.ToString()+ "# and #" + to.ToString() + "#;");
                int curr = Int32.Parse(DB.ds.Tables[0].Rows[0].ItemArray[0].ToString());
                if (min > curr || first)
                {
                    first = false;
                    min = curr;
                    minID = currWorkerID;
                }
                db.Query("Select * from Workers");
            }

            if (minID != "")
            {
                db.Query("Select * from Workers where WorkerID=" + minID + ";");
                label18.Hide();
                label7.Text = DB.ds.Tables[0].Rows[0]["WorkerID"].ToString();
                label8.Text = DB.ds.Tables[0].Rows[0]["WorkerName"].ToString();
                label9.Text = DB.ds.Tables[0].Rows[0]["Age"].ToString();
                label10.Text = DB.ds.Tables[0].Rows[0]["JoinningDate"].ToString();
                label11.Text = DB.ds.Tables[0].Rows[0]["WorkerPosition"].ToString();
                label12.Text = DB.ds.Tables[0].Rows[0]["SalaryPerHour"].ToString();
                label15.Text = DB.ds.Tables[0].Rows[0]["Password"].ToString();
                label17.Text = DB.ds.Tables[0].Rows[0]["Email"].ToString();
            }
            else
            {

                foreach (Label l in this.Controls.OfType<Label>().ToList())
                    l.Hide();
                label18.Show();
            }

        }
    }
}
