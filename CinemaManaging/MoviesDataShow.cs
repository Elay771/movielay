﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class MoviesDataShow : Form
    {
        DB db;
        Form sender;
        string WorkerID;
        public MoviesDataShow(DB db, Form sender, string WorkerID)
        {
            this.sender = sender;
            this.WorkerID = WorkerID;
            this.db = db;
            InitializeComponent();
        }

        private void MoviesDataShow_Load(object sender, EventArgs e)
        {
            
            RefreshMoviesTable();
        }
        private void RefreshMoviesTable()
        {
            db.Query("Select * from Movies");
            dataGridView1.DataSource = DB.ds.Tables[0];
            dataGridView1_CellContentClick(this, new DataGridViewCellEventArgs(0, 0));
        }

        private void Refresh_Click_1(object sender, EventArgs e)
        {
            RefreshMoviesTable();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            int index = 0;
            if (dataGridView1.SelectedRows.Count == 0 && dataGridView1.RowCount > 0)
                dataGridView1.Rows[0].Selected = true;
            if (dataGridView1.SelectedRows.Count > 0 && dataGridView1.RowCount > 0)
                index = dataGridView1.SelectedRows[0].Index;
            if (textBox1.Text != "" && textBox2.Text != "" &&
                textBox3.Text != "")
            {
                bool isSup = false;
                db.Query("Select * from Movies");
                for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
                {
                    if (dataGridView1.SelectedRows[0].Cells[0].Value.ToString() == DB.ds.Tables[0].Rows[i]["MovieID"].ToString())
                    {
                        isSup = true;
                        break;
                    }

                }
                if (isSup)
                {
                    db.Query("Select * from Movies");
                    List<string> statements = new List<string>();
                    statements.Add("UPDATE Movies new SET [MovieName]='" + textBox2.Text + "' WHERE MovieID =" + dataGridView1.SelectedRows[0].Cells[0].Value.ToString());//פעולת העידכון
                    statements.Add("UPDATE Movies new SET [Genre]='" + textBox3.Text + "' WHERE MovieID =" + dataGridView1.SelectedRows[0].Cells[0].Value.ToString());//פעולת העידכון
                    statements.Add("UPDATE Movies new SET [AgeLimit]=" + textBox4.Text + " WHERE MovieID =" + dataGridView1.SelectedRows[0].Cells[0].Value.ToString());//פעולת העידכון
                    statements.Add("UPDATE Movies new SET [RunningTimeInMinutes]=" + textBox5.Text + " WHERE MovieID =" + dataGridView1.SelectedRows[0].Cells[0].Value.ToString());//פעולת העידכון
                   for (int i = 0; i < 4; i++)
                        db.Query(statements.ToArray()[i]);
                    MessageBox.Show("Movie updated succesfully");
                    RefreshMoviesTable();
                }
                else
                    MessageBox.Show("Movie with ID: " + textBox1.Text + " is not in the system");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
            int row = e.RowIndex;
            if (row > -1)
            {
                dataGridView1.ClearSelection();
                dataGridView1.Rows[row].Selected = true;
                textBox1.Text = dataGridView1.Rows[row].Cells[0].Value.ToString();
                textBox2.Text = dataGridView1.Rows[row].Cells[1].Value.ToString();
                textBox3.Text = dataGridView1.Rows[row].Cells[2].Value.ToString();
                textBox4.Text = dataGridView1.Rows[row].Cells[3].Value.ToString();
                textBox5.Text = dataGridView1.Rows[row].Cells[4].Value.ToString();
            }

        }

        private void Delete(object sender, EventArgs e)
        {
            int index = 0;
            if (dataGridView1.SelectedRows.Count > 0 && dataGridView1.RowCount > 0)
                index = dataGridView1.SelectedRows[0].Index;
            string strSql = "DELETE FROM Movies WHERE MovieID =" + dataGridView1.SelectedRows[0].Cells[0].Value.ToString() + "";
            db.Query(strSql);
            MessageBox.Show("Movie with ID " + dataGridView1.SelectedRows[0].Cells[0].Value.ToString() + " deleted");
            RefreshMoviesTable();
        }

        private void Back(object sender, EventArgs e)
        {
            this.sender.Show();
            this.Hide();
        }

    }
}
