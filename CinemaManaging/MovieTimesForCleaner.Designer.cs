﻿namespace CinemaManaging
{
    partial class MovieTimesForCleaner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.myDB = new CinemaManaging.MyDB();
            this.theaterMoviesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.theaterMoviesTableAdapter = new CinemaManaging.MyDBTableAdapters.TheaterMoviesTableAdapter();
            this.theaterMovieIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.theaterIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.movieIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button3 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.myDB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.theaterMoviesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.theaterMovieIDDataGridViewTextBoxColumn,
            this.theaterIDDataGridViewTextBoxColumn,
            this.startTimeDataGridViewTextBoxColumn,
            this.movieIDDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.theaterMoviesBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(141, 74);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(351, 372);
            this.dataGridView1.TabIndex = 0;
            // 
            // myDB
            // 
            this.myDB.DataSetName = "MyDB";
            this.myDB.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // theaterMoviesBindingSource
            // 
            this.theaterMoviesBindingSource.DataMember = "TheaterMovies";
            this.theaterMoviesBindingSource.DataSource = this.myDB;
            // 
            // theaterMoviesTableAdapter
            // 
            this.theaterMoviesTableAdapter.ClearBeforeFill = true;
            // 
            // theaterMovieIDDataGridViewTextBoxColumn
            // 
            this.theaterMovieIDDataGridViewTextBoxColumn.DataPropertyName = "TheaterMovieID";
            this.theaterMovieIDDataGridViewTextBoxColumn.HeaderText = "TheaterMovieID";
            this.theaterMovieIDDataGridViewTextBoxColumn.Name = "theaterMovieIDDataGridViewTextBoxColumn";
            this.theaterMovieIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // theaterIDDataGridViewTextBoxColumn
            // 
            this.theaterIDDataGridViewTextBoxColumn.DataPropertyName = "TheaterID";
            this.theaterIDDataGridViewTextBoxColumn.HeaderText = "TheaterID";
            this.theaterIDDataGridViewTextBoxColumn.Name = "theaterIDDataGridViewTextBoxColumn";
            // 
            // startTimeDataGridViewTextBoxColumn
            // 
            this.startTimeDataGridViewTextBoxColumn.DataPropertyName = "StartTime";
            this.startTimeDataGridViewTextBoxColumn.HeaderText = "StartTime";
            this.startTimeDataGridViewTextBoxColumn.Name = "startTimeDataGridViewTextBoxColumn";
            // 
            // movieIDDataGridViewTextBoxColumn
            // 
            this.movieIDDataGridViewTextBoxColumn.DataPropertyName = "MovieID";
            this.movieIDDataGridViewTextBoxColumn.HeaderText = "MovieID";
            this.movieIDDataGridViewTextBoxColumn.Name = "movieIDDataGridViewTextBoxColumn";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(713, 415);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 45;
            this.button3.Text = "Back";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // MovieTimesForCleaner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.dataGridView1);
            this.Name = "MovieTimesForCleaner";
            this.Load += new System.EventHandler(this.MovieTimesForCleaner_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.myDB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.theaterMoviesBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private MyDB myDB;
        private System.Windows.Forms.BindingSource theaterMoviesBindingSource;
        private MyDBTableAdapters.TheaterMoviesTableAdapter theaterMoviesTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn theaterMovieIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn theaterIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn movieIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button button3;
    }
}