﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class Cleaner : Form
    {
        DB db;
        string WorkerID;
        public Cleaner(DB db, string WorkerID)
        {
            this.WorkerID = WorkerID;
            this.db = db;
            InitializeComponent();
        }

        private void Cleaner_Load(object sender, EventArgs e)
        {

        }

        private void LogOut_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new Start(db);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void YourData_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new WorkerData(db, this, WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void YourShifts_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new YourShifts(db, WorkerID,this);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void YourSalary(object sender, EventArgs e)
        {
            this.Hide();
            var next = new CalculateSalary(db, WorkerID, this);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void MovieTimes_Click(object sender, EventArgs e)
        {
            this.Hide();
            DateTime date = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day);
            var next = new MovieTimesForCleaner(db, this, WorkerID, date);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }
    }
}
