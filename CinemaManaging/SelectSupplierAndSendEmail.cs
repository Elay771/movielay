﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class SelectSupplierAndSendEmail : Form
    {
        DB db;
        Form prev;
        string subject;
        string content;
        string WorkerID;
        Form prevOfOrderScreen;
        public SelectSupplierAndSendEmail(string WorkerID,DB db,Form prev,string subject, string content, Form prevOfOrderScreen)
        {
            this.prevOfOrderScreen = prevOfOrderScreen;
            this.WorkerID = WorkerID;
            this.db= db;
            this.prev= prev;
            this.subject=subject;
            this.content=content;
            
            InitializeComponent();
        }

        private void SendEmailClick(object sender, EventArgs e)
        {
            Program.SendEmail(new List<string>() { dataGridView1.SelectedRows[0].Cells[2].Value.ToString() }, subject, content);
            this.Hide();
            Form next;
            if (subject.Contains("Uniform"))
                 next = new OrderUniform(new Dictionary<string, int>(), db, WorkerID, prevOfOrderScreen);
            else if (subject.Contains("Peripheral"))
                 next = new OrderPeripheralEquipment(db, WorkerID, prevOfOrderScreen);
            else
                 next = new OrderSupply(db, WorkerID, prevOfOrderScreen);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void SelectSupplierAndSendEmail_Load(object sender, EventArgs e)
        {
            if (subject.Contains("Uniform"))
                db.Query("Select * from Suppliers where SupType='Uniform'");
            else if (subject.Contains("Peripheral"))
                db.Query("Select * from Suppliers where SupType='Peripheral'");
            else if (subject.Contains("Court"))
                db.Query("Select * from Suppliers where SupType='Court'");
            dataGridView1.DataSource = DB.ds.Tables[0];
            dataGridView1.Rows[0].Selected = true;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = e.RowIndex;
            if (row > -1)
            {
                dataGridView1.ClearSelection();
                dataGridView1.Rows[row].Selected = true;
            }
        }
    }
}
