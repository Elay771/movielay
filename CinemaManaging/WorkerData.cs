﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class WorkerData : Form
    {
        DB db;
        Form sender;
        string WorkerID;
        public WorkerData(DB db, Form sender, string WorkerID)
        {
            this.sender = sender;
            this.WorkerID = WorkerID;
            this.db = db;
            InitializeComponent();
        }

        private void WorkerData_Load(object sender, EventArgs e)
        {
            db.Query("Select * from Workers where WorkerID=" + WorkerID);
            label9.Text = DB.ds.Tables[0].Rows[0]["WorkerID"].ToString();
            label10.Text = DB.ds.Tables[0].Rows[0]["WorkerName"].ToString();
            label11.Text = DB.ds.Tables[0].Rows[0]["Age"].ToString();
            label12.Text = DB.ds.Tables[0].Rows[0]["JoinningDate"].ToString();
            label13.Text = DB.ds.Tables[0].Rows[0]["WorkerPosition"].ToString();
            label14.Text = DB.ds.Tables[0].Rows[0]["SalaryPerHour"].ToString();
            label15.Text = DB.ds.Tables[0].Rows[0]["Password"].ToString();
            label16.Text = DB.ds.Tables[0].Rows[0]["Email"].ToString();
            }

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            this.sender.Show();
        }
    }
}
