﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class ShiftManagers : Form
    {
        DB db;
        Form sender;
        string WorkerID;
        public ShiftManagers(DB db, Form sender, string WorkerID)
        {
            this.sender = sender;
            this.WorkerID = WorkerID;
            this.db = db;
            InitializeComponent();
        }

        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = ((ListBox)sender).SelectedIndex;
            Program.changeSelectedIndex(index, this.Controls.OfType<ListBox>().ToList());// change selected index in all lists
        }

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            this.sender.Show();
        }

        private void ShiftManagers_Load(object sender, EventArgs e)
        {
            foreach (DataRow i in DB.ds.Tables[0].Rows)
            {
                string workerID = i["WorkerID"].ToString();
                listBox1.Items.Add(workerID);
                db.Query("Select WorkerName from Workers where WorkerID=" + workerID + " Where WorkerPosition='Shift Manager'");
                listBox2.Items.Add(DB.ds.Tables[0].Rows[0]["WorkerName"]);
            }
        }
    }
}
