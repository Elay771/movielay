﻿namespace CinemaManaging
{
    partial class NewWorker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.EnteredName = new System.Windows.Forms.TextBox();
            this.EnteredWorkerPosition = new System.Windows.Forms.TextBox();
            this.EnteredAge = new System.Windows.Forms.TextBox();
            this.EnteredID = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.EnteredPassword = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.JoinError = new System.Windows.Forms.Label();
            this.EnteredEmail = new System.Windows.Forms.TextBox();
            this.Emaillabel = new System.Windows.Forms.Label();
            this.BackToMenu = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(-2, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(609, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome to Our Team, Please Enter Your Details";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(61, 145);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(78, 170);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "ID";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(70, 193);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Age";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(52, 215);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "WorkerPosition";
            // 
            // EnteredName
            // 
            this.EnteredName.Location = new System.Drawing.Point(119, 145);
            this.EnteredName.Margin = new System.Windows.Forms.Padding(2);
            this.EnteredName.Name = "EnteredName";
            this.EnteredName.Size = new System.Drawing.Size(241, 20);
            this.EnteredName.TabIndex = 5;
            // 
            // EnteredWorkerPosition
            // 
            this.EnteredWorkerPosition.Location = new System.Drawing.Point(119, 215);
            this.EnteredWorkerPosition.Margin = new System.Windows.Forms.Padding(2);
            this.EnteredWorkerPosition.Name = "EnteredWorkerPosition";
            this.EnteredWorkerPosition.Size = new System.Drawing.Size(241, 20);
            this.EnteredWorkerPosition.TabIndex = 6;
            // 
            // EnteredAge
            // 
            this.EnteredAge.Location = new System.Drawing.Point(119, 193);
            this.EnteredAge.Margin = new System.Windows.Forms.Padding(2);
            this.EnteredAge.Name = "EnteredAge";
            this.EnteredAge.Size = new System.Drawing.Size(241, 20);
            this.EnteredAge.TabIndex = 7;
            // 
            // EnteredID
            // 
            this.EnteredID.Location = new System.Drawing.Point(119, 170);
            this.EnteredID.Margin = new System.Windows.Forms.Padding(2);
            this.EnteredID.Name = "EnteredID";
            this.EnteredID.Size = new System.Drawing.Size(241, 20);
            this.EnteredID.TabIndex = 8;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(120, 286);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(69, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Join";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.joinButtonClick);
            // 
            // EnteredPassword
            // 
            this.EnteredPassword.Location = new System.Drawing.Point(119, 239);
            this.EnteredPassword.Margin = new System.Windows.Forms.Padding(2);
            this.EnteredPassword.Name = "EnteredPassword";
            this.EnteredPassword.Size = new System.Drawing.Size(241, 20);
            this.EnteredPassword.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(43, 239);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Password";
            // 
            // JoinError
            // 
            this.JoinError.AutoSize = true;
            this.JoinError.ForeColor = System.Drawing.Color.Red;
            this.JoinError.Location = new System.Drawing.Point(116, 316);
            this.JoinError.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.JoinError.Name = "JoinError";
            this.JoinError.Size = new System.Drawing.Size(52, 13);
            this.JoinError.TabIndex = 12;
            this.JoinError.Text = "Try Again";
            // 
            // EnteredEmail
            // 
            this.EnteredEmail.Location = new System.Drawing.Point(120, 261);
            this.EnteredEmail.Name = "EnteredEmail";
            this.EnteredEmail.Size = new System.Drawing.Size(240, 20);
            this.EnteredEmail.TabIndex = 13;
            // 
            // Emaillabel
            // 
            this.Emaillabel.AutoSize = true;
            this.Emaillabel.Location = new System.Drawing.Point(61, 268);
            this.Emaillabel.Name = "Emaillabel";
            this.Emaillabel.Size = new System.Drawing.Size(32, 13);
            this.Emaillabel.TabIndex = 14;
            this.Emaillabel.Text = "Email";
            // 
            // BackToMenu
            // 
            this.BackToMenu.Location = new System.Drawing.Point(501, 325);
            this.BackToMenu.Name = "BackToMenu";
            this.BackToMenu.Size = new System.Drawing.Size(87, 40);
            this.BackToMenu.TabIndex = 15;
            this.BackToMenu.Text = "Back to Menu";
            this.BackToMenu.UseVisualStyleBackColor = true;
            this.BackToMenu.Click += new System.EventHandler(this.BackToMenu_Click);
            // 
            // NewWorker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.BackToMenu);
            this.Controls.Add(this.Emaillabel);
            this.Controls.Add(this.EnteredEmail);
            this.Controls.Add(this.JoinError);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.EnteredPassword);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.EnteredID);
            this.Controls.Add(this.EnteredAge);
            this.Controls.Add(this.EnteredWorkerPosition);
            this.Controls.Add(this.EnteredName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "NewWorker";
            this.Load += new System.EventHandler(this.NewWorker_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox EnteredName;
        private System.Windows.Forms.TextBox EnteredWorkerPosition;
        private System.Windows.Forms.TextBox EnteredAge;
        private System.Windows.Forms.TextBox EnteredID;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox EnteredPassword;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label JoinError;
        private System.Windows.Forms.TextBox EnteredEmail;
        private System.Windows.Forms.Label Emaillabel;
        private System.Windows.Forms.Button BackToMenu;
    }
}