﻿namespace CinemaManaging
{
    partial class ChooseMovieDate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.listBox4 = new System.Windows.Forms.ListBox();
            this.dataBaseDataSet = new CinemaManaging.DataBaseDataSet();
            this.movieOrdersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.movieOrdersTableAdapter = new CinemaManaging.DataBaseDataSetTableAdapters.MovieOrdersTableAdapter();
            this.theaterMoviesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.theaterMoviesTableAdapter = new CinemaManaging.DataBaseDataSetTableAdapters.TheaterMoviesTableAdapter();
            this.movieIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startTimeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.theaterIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.theaterMovieIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataBaseDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.movieOrdersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.theaterMoviesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(182, 278);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(56, 19);
            this.button1.TabIndex = 6;
            this.button1.Text = "Next";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Next);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 331);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "Back";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Back);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label4.Location = new System.Drawing.Point(13, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(252, 31);
            this.label4.TabIndex = 16;
            this.label4.Text = "Choose Movie Date";
            // 
            // listBox4
            // 
            this.listBox4.FormattingEnabled = true;
            this.listBox4.Location = new System.Drawing.Point(578, 344);
            this.listBox4.Name = "listBox4";
            this.listBox4.Size = new System.Drawing.Size(10, 4);
            this.listBox4.TabIndex = 7;
            this.listBox4.Visible = false;
            // 
            // dataBaseDataSet
            // 
            this.dataBaseDataSet.DataSetName = "DataBaseDataSet";
            this.dataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // movieOrdersBindingSource
            // 
            this.movieOrdersBindingSource.DataMember = "MovieOrders";
            this.movieOrdersBindingSource.DataSource = this.dataBaseDataSet;
            // 
            // movieOrdersTableAdapter
            // 
            this.movieOrdersTableAdapter.ClearBeforeFill = true;
            // 
            // theaterMoviesBindingSource
            // 
            this.theaterMoviesBindingSource.DataMember = "TheaterMovies";
            this.theaterMoviesBindingSource.DataSource = this.dataBaseDataSet;
            // 
            // theaterMoviesTableAdapter
            // 
            this.theaterMoviesTableAdapter.ClearBeforeFill = true;
            // 
            // movieIDDataGridViewTextBoxColumn
            // 
            this.movieIDDataGridViewTextBoxColumn.DataPropertyName = "MovieID";
            this.movieIDDataGridViewTextBoxColumn.HeaderText = "Movie";
            this.movieIDDataGridViewTextBoxColumn.Name = "movieIDDataGridViewTextBoxColumn";
            // 
            // startTimeDataGridViewTextBoxColumn
            // 
            this.startTimeDataGridViewTextBoxColumn.DataPropertyName = "StartTime";
            this.startTimeDataGridViewTextBoxColumn.HeaderText = "Date";
            this.startTimeDataGridViewTextBoxColumn.Name = "startTimeDataGridViewTextBoxColumn";
            // 
            // theaterIDDataGridViewTextBoxColumn
            // 
            this.theaterIDDataGridViewTextBoxColumn.DataPropertyName = "TheaterID";
            this.theaterIDDataGridViewTextBoxColumn.HeaderText = "Theater";
            this.theaterIDDataGridViewTextBoxColumn.Name = "theaterIDDataGridViewTextBoxColumn";
            // 
            // theaterMovieIDDataGridViewTextBoxColumn
            // 
            this.theaterMovieIDDataGridViewTextBoxColumn.DataPropertyName = "TheaterMovieID";
            this.theaterMovieIDDataGridViewTextBoxColumn.HeaderText = "TheaterMovieID";
            this.theaterMovieIDDataGridViewTextBoxColumn.Name = "theaterMovieIDDataGridViewTextBoxColumn";
            this.theaterMovieIDDataGridViewTextBoxColumn.Visible = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.theaterMovieIDDataGridViewTextBoxColumn,
            this.theaterIDDataGridViewTextBoxColumn,
            this.startTimeDataGridViewTextBoxColumn,
            this.movieIDDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.theaterMoviesBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(3, 96);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(557, 150);
            this.dataGridView1.TabIndex = 17;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // ChooseMovieDate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.listBox4);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "ChooseMovieDate";
            this.Load += new System.EventHandler(this.ChooseMovieDate_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataBaseDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.movieOrdersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.theaterMoviesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listBox4;
        private DataBaseDataSet dataBaseDataSet;
        private System.Windows.Forms.BindingSource movieOrdersBindingSource;
        private DataBaseDataSetTableAdapters.MovieOrdersTableAdapter movieOrdersTableAdapter;
        private System.Windows.Forms.BindingSource theaterMoviesBindingSource;
        private DataBaseDataSetTableAdapters.TheaterMoviesTableAdapter theaterMoviesTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn movieIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startTimeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn theaterIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn theaterMovieIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}