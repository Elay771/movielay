﻿namespace CinemaManaging
{
    partial class AssistantManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HiManager = new System.Windows.Forms.Label();
            this.WorkersData = new System.Windows.Forms.Button();
            this.CustomersData = new System.Windows.Forms.Button();
            this.OrderTicket = new System.Windows.Forms.Button();
            this.Statistics = new System.Windows.Forms.Button();
            this.TheatersData = new System.Windows.Forms.Button();
            this.MoviesData = new System.Windows.Forms.Button();
            this.LogOut = new System.Windows.Forms.Button();
            this.ShiftsOfTheWeek = new System.Windows.Forms.Button();
            this.YourSalary = new System.Windows.Forms.Button();
            this.YourShifts = new System.Windows.Forms.Button();
            this.YourData = new System.Windows.Forms.Button();
            this.OrderingMenu = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // HiManager
            // 
            this.HiManager.AutoSize = true;
            this.HiManager.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.HiManager.Location = new System.Drawing.Point(32, 39);
            this.HiManager.Name = "HiManager";
            this.HiManager.Size = new System.Drawing.Size(288, 31);
            this.HiManager.TabIndex = 9;
            this.HiManager.Text = "Hi, Assistant Manager!";
            // 
            // WorkersData
            // 
            this.WorkersData.Location = new System.Drawing.Point(12, 319);
            this.WorkersData.Name = "WorkersData";
            this.WorkersData.Size = new System.Drawing.Size(144, 23);
            this.WorkersData.TabIndex = 8;
            this.WorkersData.Text = "Workers Data";
            this.WorkersData.UseVisualStyleBackColor = true;
            this.WorkersData.Click += new System.EventHandler(this.WorkersData_Click);
            // 
            // CustomersData
            // 
            this.CustomersData.Location = new System.Drawing.Point(12, 290);
            this.CustomersData.Name = "CustomersData";
            this.CustomersData.Size = new System.Drawing.Size(144, 23);
            this.CustomersData.TabIndex = 7;
            this.CustomersData.Text = "Customers Data";
            this.CustomersData.UseVisualStyleBackColor = true;
            this.CustomersData.Click += new System.EventHandler(this.CustomersData_Click);
            // 
            // OrderTicket
            // 
            this.OrderTicket.Location = new System.Drawing.Point(12, 232);
            this.OrderTicket.Name = "OrderTicket";
            this.OrderTicket.Size = new System.Drawing.Size(144, 23);
            this.OrderTicket.TabIndex = 6;
            this.OrderTicket.Text = "Order a Movie Ticket";
            this.OrderTicket.UseVisualStyleBackColor = true;
            this.OrderTicket.Click += new System.EventHandler(this.OrderTicket_Click);
            // 
            // Statistics
            // 
            this.Statistics.Location = new System.Drawing.Point(12, 203);
            this.Statistics.Name = "Statistics";
            this.Statistics.Size = new System.Drawing.Size(144, 23);
            this.Statistics.TabIndex = 5;
            this.Statistics.Text = "View Statistics";
            this.Statistics.UseVisualStyleBackColor = true;
            this.Statistics.Click += new System.EventHandler(this.Statistics_Click);
            // 
            // TheatersData
            // 
            this.TheatersData.Location = new System.Drawing.Point(12, 378);
            this.TheatersData.Name = "TheatersData";
            this.TheatersData.Size = new System.Drawing.Size(144, 23);
            this.TheatersData.TabIndex = 11;
            this.TheatersData.Text = "Theaters Data";
            this.TheatersData.UseVisualStyleBackColor = true;
            this.TheatersData.Click += new System.EventHandler(this.TheatersData_Click);
            // 
            // MoviesData
            // 
            this.MoviesData.Location = new System.Drawing.Point(12, 348);
            this.MoviesData.Name = "MoviesData";
            this.MoviesData.Size = new System.Drawing.Size(144, 23);
            this.MoviesData.TabIndex = 10;
            this.MoviesData.Text = "Movies Data";
            this.MoviesData.UseVisualStyleBackColor = true;
            this.MoviesData.Click += new System.EventHandler(this.MoviesData_Click);
            // 
            // LogOut
            // 
            this.LogOut.Location = new System.Drawing.Point(701, 398);
            this.LogOut.Name = "LogOut";
            this.LogOut.Size = new System.Drawing.Size(87, 40);
            this.LogOut.TabIndex = 18;
            this.LogOut.Text = "Log Out";
            this.LogOut.UseVisualStyleBackColor = true;
            this.LogOut.Click += new System.EventHandler(this.LogOut_Click);
            // 
            // ShiftsOfTheWeek
            // 
            this.ShiftsOfTheWeek.Location = new System.Drawing.Point(13, 407);
            this.ShiftsOfTheWeek.Name = "ShiftsOfTheWeek";
            this.ShiftsOfTheWeek.Size = new System.Drawing.Size(143, 23);
            this.ShiftsOfTheWeek.TabIndex = 24;
            this.ShiftsOfTheWeek.Text = "Manage Shifts";
            this.ShiftsOfTheWeek.UseVisualStyleBackColor = true;
            this.ShiftsOfTheWeek.Click += new System.EventHandler(this.ManageShifts_Click);
            // 
            // YourSalary
            // 
            this.YourSalary.Location = new System.Drawing.Point(12, 116);
            this.YourSalary.Name = "YourSalary";
            this.YourSalary.Size = new System.Drawing.Size(144, 23);
            this.YourSalary.TabIndex = 23;
            this.YourSalary.Text = "Your Salary";
            this.YourSalary.UseVisualStyleBackColor = true;
            this.YourSalary.Click += new System.EventHandler(this.YourSalary_Click);
            // 
            // YourShifts
            // 
            this.YourShifts.Location = new System.Drawing.Point(12, 145);
            this.YourShifts.Name = "YourShifts";
            this.YourShifts.Size = new System.Drawing.Size(144, 23);
            this.YourShifts.TabIndex = 22;
            this.YourShifts.Text = "Your Shifts";
            this.YourShifts.UseVisualStyleBackColor = true;
            this.YourShifts.Click += new System.EventHandler(this.YourShifts_Click);
            // 
            // YourData
            // 
            this.YourData.Location = new System.Drawing.Point(12, 87);
            this.YourData.Name = "YourData";
            this.YourData.Size = new System.Drawing.Size(144, 23);
            this.YourData.TabIndex = 21;
            this.YourData.Text = "Your Data";
            this.YourData.UseVisualStyleBackColor = true;
            this.YourData.Click += new System.EventHandler(this.YourData_Click);
            // 
            // OrderingMenu
            // 
            this.OrderingMenu.Location = new System.Drawing.Point(12, 261);
            this.OrderingMenu.Name = "OrderingMenu";
            this.OrderingMenu.Size = new System.Drawing.Size(144, 23);
            this.OrderingMenu.TabIndex = 27;
            this.OrderingMenu.Text = "Food Court Menu";
            this.OrderingMenu.UseVisualStyleBackColor = true;
            this.OrderingMenu.Click += new System.EventHandler(this.OrderingMenu_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 174);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(144, 23);
            this.button1.TabIndex = 43;
            this.button1.Text = "Order Items";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.SupOrder);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(162, 87);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(97, 52);
            this.button2.TabIndex = 44;
            this.button2.Text = "Send Notification to Workers";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.NotificationClick);
            // 
            // AssistantManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.OrderingMenu);
            this.Controls.Add(this.ShiftsOfTheWeek);
            this.Controls.Add(this.YourSalary);
            this.Controls.Add(this.YourShifts);
            this.Controls.Add(this.YourData);
            this.Controls.Add(this.LogOut);
            this.Controls.Add(this.TheatersData);
            this.Controls.Add(this.MoviesData);
            this.Controls.Add(this.HiManager);
            this.Controls.Add(this.WorkersData);
            this.Controls.Add(this.CustomersData);
            this.Controls.Add(this.OrderTicket);
            this.Controls.Add(this.Statistics);
            this.Name = "AssistantManager";
            this.Load += new System.EventHandler(this.AssistantManager_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HiManager;
        private System.Windows.Forms.Button WorkersData;
        private System.Windows.Forms.Button CustomersData;
        private System.Windows.Forms.Button OrderTicket;
        private System.Windows.Forms.Button Statistics;
        private System.Windows.Forms.Button TheatersData;
        private System.Windows.Forms.Button MoviesData;
        private System.Windows.Forms.Button LogOut;
        private System.Windows.Forms.Button ShiftsOfTheWeek;
        private System.Windows.Forms.Button YourSalary;
        private System.Windows.Forms.Button YourShifts;
        private System.Windows.Forms.Button YourData;
        private System.Windows.Forms.Button OrderingMenu;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}