﻿namespace CinemaManaging
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelLogin = new System.Windows.Forms.Label();
            this.EnteredUsername = new System.Windows.Forms.TextBox();
            this.EnteredPassword = new System.Windows.Forms.TextBox();
            this.CheckLogin = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.LoginErrorLabel = new System.Windows.Forms.Label();
            this.BackToMenu = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelLogin
            // 
            this.labelLogin.AutoSize = true;
            this.labelLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelLogin.Location = new System.Drawing.Point(11, 9);
            this.labelLogin.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(150, 31);
            this.labelLogin.TabIndex = 0;
            this.labelLogin.Text = "Hi, Worker!";
            // 
            // EnteredUsername
            // 
            this.EnteredUsername.Location = new System.Drawing.Point(116, 170);
            this.EnteredUsername.Margin = new System.Windows.Forms.Padding(2);
            this.EnteredUsername.Name = "EnteredUsername";
            this.EnteredUsername.Size = new System.Drawing.Size(212, 20);
            this.EnteredUsername.TabIndex = 1;
            // 
            // EnteredPassword
            // 
            this.EnteredPassword.Location = new System.Drawing.Point(116, 194);
            this.EnteredPassword.Margin = new System.Windows.Forms.Padding(2);
            this.EnteredPassword.Name = "EnteredPassword";
            this.EnteredPassword.PasswordChar = '•';
            this.EnteredPassword.Size = new System.Drawing.Size(212, 20);
            this.EnteredPassword.TabIndex = 2;
            // 
            // CheckLogin
            // 
            this.CheckLogin.Location = new System.Drawing.Point(116, 226);
            this.CheckLogin.Margin = new System.Windows.Forms.Padding(2);
            this.CheckLogin.Name = "CheckLogin";
            this.CheckLogin.Size = new System.Drawing.Size(83, 28);
            this.CheckLogin.TabIndex = 3;
            this.CheckLogin.Text = "Log In";
            this.CheckLogin.UseVisualStyleBackColor = true;
            this.CheckLogin.Click += new System.EventHandler(this.CheckLogin_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(94, 175);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(58, 197);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Password";
            // 
            // LoginErrorLabel
            // 
            this.LoginErrorLabel.AutoSize = true;
            this.LoginErrorLabel.ForeColor = System.Drawing.Color.Red;
            this.LoginErrorLabel.Location = new System.Drawing.Point(61, 292);
            this.LoginErrorLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LoginErrorLabel.Name = "LoginErrorLabel";
            this.LoginErrorLabel.Size = new System.Drawing.Size(153, 13);
            this.LoginErrorLabel.TabIndex = 6;
            this.LoginErrorLabel.Text = "Invalid Username or Password.";
            // 
            // BackToMenu
            // 
            this.BackToMenu.Location = new System.Drawing.Point(501, 323);
            this.BackToMenu.Name = "BackToMenu";
            this.BackToMenu.Size = new System.Drawing.Size(87, 40);
            this.BackToMenu.TabIndex = 16;
            this.BackToMenu.Text = "Back to Menu";
            this.BackToMenu.UseVisualStyleBackColor = true;
            this.BackToMenu.Click += new System.EventHandler(this.BackToMenu_Click);
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.BackToMenu);
            this.Controls.Add(this.LoginErrorLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CheckLogin);
            this.Controls.Add(this.EnteredPassword);
            this.Controls.Add(this.EnteredUsername);
            this.Controls.Add(this.labelLogin);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Login";
            this.Load += new System.EventHandler(this.Login_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.TextBox EnteredUsername;
        private System.Windows.Forms.TextBox EnteredPassword;
        private System.Windows.Forms.Button CheckLogin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LoginErrorLabel;
        private System.Windows.Forms.Button BackToMenu;
    }
}