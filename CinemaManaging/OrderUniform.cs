﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class OrderUniform : Form
    {
        DB db;
        string WorkerID;
        Form prev;
        Dictionary<string, int> sizeAndAmount;
        List<string> sizes = new List<string>() {"XS","S","L","M","XL","XXL","XXXL"};
        public OrderUniform(Dictionary<string, int> sizeAndAmount, DB db, string WorkerID, Form prev)// Previous form workerid and datebase
        {
            this.sizeAndAmount = new Dictionary<string, int>();
            this.db = db;
            this.WorkerID = WorkerID;
            this.prev = prev;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            db.Query("Select * from Workers where WorkerID=" + WorkerID);
            string workerName = DB.ds.Tables[0].Rows[0]["WorkerName"].ToString();
            string content = "Hi, Supplier\nMoviElay Cinema Would Like to Order:\n";
            foreach (string size in sizeAndAmount.Keys)
                content+= sizeAndAmount[size]+" Uniforms at Size: "+size+"\n";
            content += "Thank You, " + workerName+ " from MoviElay Cinema";
            this.Hide();
            var next = new SelectSupplierAndSendEmail(WorkerID, db,this,"Uniform Order From MoviElay",content, prev);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }


        private void AddToOrder(object sender, EventArgs e)
        {
            try
            {
                int amount = Int32.Parse(AmountTextBox.Text);
                string size = SizeTextBox.Text;
                SizeTextBox.Text = "";
                AmountTextBox.Text = "";
                if (amount <= 0)
                    MessageBox.Show("Error! Invalid Amount");
                else if (!sizes.Contains(size.ToUpper()))
                    MessageBox.Show("Error! Invalid Size");
                else if (!sizeAndAmount.ContainsKey(size))
                    sizeAndAmount.Add(size, amount);
                else
                    sizeAndAmount[size] += amount;
            }
            catch
            {
                MessageBox.Show("Error! Invalid Amount");
            }
        }

        private void OrderUniform_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            prev.Show();
        }
    }
}
