﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class WorkersDataShow : Form
    {
        public WorkersDataShow()
        {
            InitializeComponent();
        }
        DB db;
        Form sender;
        string WorkerID;
        public WorkersDataShow(DB db, Form sender, string WorkerID)
        {
            this.sender = sender;
            this.WorkerID = WorkerID;
            this.db = db;
            InitializeComponent();
        }



        private void WorkersDataShow_Load(object sender, EventArgs e)
        {
            RefreshWorkersTable();
        }
        private void RefreshWorkersTable()
        {
            db.WorkersData();
            dataGridView1.DataSource = DB.ds.Tables[0];
            dataGridView1.Rows[0].Selected = true;

        }
        private void Refresh_Click(object sender, EventArgs e)
        {
            RefreshWorkersTable();
        } 

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            this.sender.Show();
        }


    }
}
