﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class Statistics : Form
    {
        DB db;
        Form sender;
        string WorkerID;
        public Statistics(DB db,Form sender,string WorkerID)
        {
            this.sender = sender;
            this.WorkerID = WorkerID;
            this.db = db;
            InitializeComponent();
        }

        private void Statistics_Load(object sender, EventArgs e)
        {

        }

        private void GetStatistics(object sender, EventArgs e)
        {
            DateTime from = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 0, 0, 0);
            DateTime to = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 0, 0, 0);
            if (DateTime.Compare(from,to) <= 0)
            {
                if (radioButton1.Checked)
                {
                    this.Hide();
                    var next = new MostOrderedMovie(db,from,to, this,WorkerID);
                    next.Closed += (s, args) => this.Close();
                    next.Show();
                }

                else if (radioButton2.Checked)
                {
                    this.Hide();
                    var next = new MostOrderedTheater(db, from, to, this, WorkerID);
                    next.Closed += (s, args) => this.Close();
                    next.Show();
                }
                else if (radioButton3.Checked)
                {
                    this.Hide();
                    var next = new MostHelpfulWorker(db, from, to, this, WorkerID);
                    next.Closed += (s, args) => this.Close();
                    next.Show();
                }

                else if (radioButton4.Checked)
                {
                    this.Hide();
                    var next = new MostActiveClient(db, from, to, this, WorkerID);
                    next.Closed += (s, args) => this.Close();
                    next.Show();
                }

                else if (radioButton5.Checked)
                {
                    this.Hide();
                    var next = new LeastHelpfulWorker(db, from, to, this, WorkerID);
                    next.Closed += (s, args) => this.Close();
                    next.Show();
                }

                else if (radioButton6.Checked)
                {
                    this.Hide();
                    var next = new LeastOrderedMovie(db, from, to, this, WorkerID);
                    next.Closed += (s, args) => this.Close();
                    next.Show();
                }
            }
        }

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            this.sender.Show();
        }
    }
}
