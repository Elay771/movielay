﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class ChooseMovieDate : Form
    {
        DB db;
        string movieID;
        string WorkerID;
        Form sender;
        public ChooseMovieDate(DB db, string movieID, Form sender, string WorkerID)
        {
            this.sender = sender;
            this.WorkerID = WorkerID;
            this.movieID = movieID;
            this.db = db;
            InitializeComponent();
        }

        private void ChooseMovieDate_Load(object sender, EventArgs e)
        {
            RefreshMovieOrdersTable();
        }
        private void RefreshMovieOrdersTable()
        {
            db.Query("Select * from TheaterMovies where MovieID=" + movieID + ";");
            dataGridView1.DataSource = DB.ds.Tables[0];
            dataGridView1_CellContentClick(this, new DataGridViewCellEventArgs(0, 0));


        }
        private void Next(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0)
                dataGridView1.Rows[0].Selected=true;
            db.Query("Select * from TheaterMovies where MovieID=" + movieID + ";");
            int index = 0;
            if (dataGridView1.SelectedRows.Count > 0 && dataGridView1.RowCount > 0)
                index = dataGridView1.SelectedRows[0].Index;
            this.Hide();
            var next = new ChooseSeats(db, dataGridView1.SelectedRows[0].Cells[0].Value.ToString(), this, WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();

        }

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            this.sender.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = e.RowIndex;
            if (row > -1)
            {
                dataGridView1.ClearSelection();
                dataGridView1.Rows[row].Selected = true;
            }
        }
    }
}
