﻿namespace CinemaManaging
{
    partial class FoodCourtSeller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.YourData = new System.Windows.Forms.Button();
            this.OrderingMenu = new System.Windows.Forms.Button();
            this.YourShifts = new System.Windows.Forms.Button();
            this.LogOut = new System.Windows.Forms.Button();
            this.NotificationsMenu = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // YourData
            // 
            this.YourData.Location = new System.Drawing.Point(13, 81);
            this.YourData.Name = "YourData";
            this.YourData.Size = new System.Drawing.Size(107, 23);
            this.YourData.TabIndex = 0;
            this.YourData.Text = "Your Data";
            this.YourData.UseVisualStyleBackColor = true;
            this.YourData.Click += new System.EventHandler(this.YourData_Click);
            // 
            // OrderingMenu
            // 
            this.OrderingMenu.Location = new System.Drawing.Point(13, 110);
            this.OrderingMenu.Name = "OrderingMenu";
            this.OrderingMenu.Size = new System.Drawing.Size(107, 23);
            this.OrderingMenu.TabIndex = 1;
            this.OrderingMenu.Text = "Food Court Menu";
            this.OrderingMenu.UseVisualStyleBackColor = true;
            this.OrderingMenu.Click += new System.EventHandler(this.OrderingMenu_Click);
            // 
            // YourShifts
            // 
            this.YourShifts.Location = new System.Drawing.Point(13, 139);
            this.YourShifts.Name = "YourShifts";
            this.YourShifts.Size = new System.Drawing.Size(107, 23);
            this.YourShifts.TabIndex = 2;
            this.YourShifts.Text = "Your Shifts";
            this.YourShifts.UseVisualStyleBackColor = true;
            this.YourShifts.Click += new System.EventHandler(this.YourShifts_Click);
            // 
            // LogOut
            // 
            this.LogOut.Location = new System.Drawing.Point(701, 398);
            this.LogOut.Name = "LogOut";
            this.LogOut.Size = new System.Drawing.Size(87, 40);
            this.LogOut.TabIndex = 18;
            this.LogOut.Text = "Log Out";
            this.LogOut.UseVisualStyleBackColor = true;
            this.LogOut.Click += new System.EventHandler(this.LogOut_Click);
            // 
            // NotificationsMenu
            // 
            this.NotificationsMenu.Location = new System.Drawing.Point(13, 169);
            this.NotificationsMenu.Name = "NotificationsMenu";
            this.NotificationsMenu.Size = new System.Drawing.Size(107, 23);
            this.NotificationsMenu.TabIndex = 3;
            this.NotificationsMenu.Text = "Your Salary";
            this.NotificationsMenu.UseVisualStyleBackColor = true;
            this.NotificationsMenu.Click += new System.EventHandler(this.YourSalary_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label6.Location = new System.Drawing.Point(12, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(275, 31);
            this.label6.TabIndex = 22;
            this.label6.Text = "Hi, Food Court Seller!";
            // 
            // FoodCourtSeller
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.LogOut);
            this.Controls.Add(this.NotificationsMenu);
            this.Controls.Add(this.YourShifts);
            this.Controls.Add(this.OrderingMenu);
            this.Controls.Add(this.YourData);
            this.Name = "FoodCourtSeller";
            this.Load += new System.EventHandler(this.FoodCourtSeller_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button YourData;
        private System.Windows.Forms.Button OrderingMenu;
        private System.Windows.Forms.Button YourShifts;
        private System.Windows.Forms.Button LogOut;
        private System.Windows.Forms.Button NotificationsMenu;
        private System.Windows.Forms.Label label6;
    }
}