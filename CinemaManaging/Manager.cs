﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class Manager : Form
    {
        DB db;
        string WorkerID;
        public Manager(DB db, string WorkerID)
        {
            this.WorkerID = WorkerID;
            this.db = db;
            InitializeComponent();
        }

        private void Manager_Load(object sender, EventArgs e)
        {
        }

        private void Statistics_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new Statistics(db,this,WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void OrderMovieTicket(object sender, EventArgs e)
        {
            this.Hide();
            var next = new MovieTicketOrder(db,this,WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void CustomersData_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new CustomerDataShow(db,this, WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void WorkersData_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new WorkersDataShowAndUpdate(db,this, WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void MoviesData_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new MoviesDataShow(db, this, WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void TheatersData_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new TheatersDataShow(db, this, WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void LogOut_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new Start(db);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void YourSalary_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new CalculateSalary(db, WorkerID,this);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void YourData_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new WorkerData(db, this, WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void ShiftsOfTheWeek_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new ManageShifts(db, this, this.WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void YourShifts_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new YourShifts(db, WorkerID, this);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }



        private void OrderingMenu_Click(object sender, EventArgs e)
        {

            this.Hide();
            var next = new Food_Court_Order(db, WorkerID, this);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void OrderTicket_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new MovieTicketOrder(db, this, WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void Order_Items(object sender, EventArgs e)
        {
            this.Hide();
            var next = new OrderItems(db, WorkerID, this);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void NotificationClick(object sender, EventArgs e)
        {
            this.Hide();
            var next = new NotificationMenu(db, WorkerID, this);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }
    }
}
