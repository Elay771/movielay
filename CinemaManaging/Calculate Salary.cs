﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class CalculateSalary : Form
    {
        DB db;
        string WorkerID;
        Form prev;
        public CalculateSalary(DB db, string WorkerID, Form prev)
        {
            this.db = db;
            this.WorkerID = WorkerID;
            this.prev = prev;
            InitializeComponent();
        }

        private void CalculateSalary_Load(object sender, EventArgs e)
        {
            label3.Hide();
            label4.Hide();
        }

        private void Calc(object sender, EventArgs e)
        {
            int numShifts = 0;
            int salary = 0;
            DateTime from = new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 0, 0, 0);
            DateTime to = new DateTime(dateTimePicker2.Value.Year, dateTimePicker2.Value.Month, dateTimePicker2.Value.Day, 0, 0, 0);
            Dictionary<int, int> MoviesCounts = new Dictionary<int, int>();
            db.Query("Select COUNT(*) from Shifts WHERE WorkerID ="+WorkerID+" and StartTime between #" + from.ToString() + "# and #" + to.ToString() + "#;");
            numShifts = Int32.Parse(DB.ds.Tables[0].Rows[0].ItemArray[0].ToString());
            db.Query("Select * from Workers WHERE WorkerID =" + WorkerID);
            bool worked =Int32.TryParse(DB.ds.Tables[0].Rows[0]["SalaryPerHour"].ToString(),out salary);  // get salary of worker
            if (worked)
            {
                label3.Show();
                label3.Text = "Salary Between Dates is: " + salary*numShifts*6; // All shifts are 6 hours
            }
            else //if didn't work (salary isn't valid)
                label4.Show();
        }

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            this.prev.Show();
        }
    }
}