﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class Login : Form
    {
        DB db;
        public void LoginByAccess(string access)
        {
            if (access.CompareTo("Manager") == 0)
            {
                this.Hide();
                var next = new Manager(db,EnteredUsername.Text);
                next.Closed += (s, args) => this.Close();
                next.Show();
            }

            if (access.Contains("Shift Manager"))
            {
                this.Hide();
                var next = new ShiftManager(db, EnteredUsername.Text);
                next.Closed += (s, args) => this.Close();
                next.Show();
            }
            if (access.Contains("Cleaner"))
            {
                this.Hide();
                var next = new Cleaner(db, EnteredUsername.Text);
                next.Closed += (s, args) => this.Close();
                next.Show();
            }
            if (access.CompareTo("Assistant Manager") == 0)
            {
                this.Hide();
                var next = new AssistantManager(db, EnteredUsername.Text);
                next.Closed += (s, args) => this.Close();
                next.Show();
            }

            if (access.CompareTo("Tickets Seller") == 0)
            {
                this.Hide();
                var next = new TicketSeller(db, EnteredUsername.Text);
                next.Closed += (s, args) => this.Close();
                next.Show();
            }

            if (access.CompareTo("Food Court Seller") == 0)
            {
                this.Hide();
                var next = new FoodCourtSeller(db, EnteredUsername.Text);
                next.Closed += (s, args) => this.Close();
                next.Show();
            }

            if (access.Contains("Tickets Checker"))
            {
                this.Hide();
                var next = new TicketChecker(db, EnteredUsername.Text);
                next.Closed += (s, args) => this.Close();
                next.Show();
            }
        }

        public Login(DB db)
        {
            this.db = db;
            InitializeComponent();
            LoginErrorLabel.Hide();
        }

        private void CheckLogin_Click(object sender, EventArgs e)
        {
            LoginErrorLabel.Hide();
            string username= EnteredUsername.Text;
            string password= EnteredPassword.Text;
            if(db.isPasswordCorrect(username, password))
            {
                string access = db.getUserWorkerPosition(username);
                LoginByAccess(access);
            }
            else
            {
                EnteredPassword.Text = "";
                EnteredUsername.Text = "";
                LoginErrorLabel.Show();
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void BackToMenu_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new Start(db);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }
    }
}
