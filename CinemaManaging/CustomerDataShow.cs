﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class CustomerDataShow : Form
    {
        DB db;
        Form sender;
        string WorkerID;
        public CustomerDataShow(DB db, Form sender, string WorkerID)
        {
            this.sender = sender;
            this.WorkerID = WorkerID;
            this.db = db;
            InitializeComponent();
        }

        private void CustomerDataShow_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'dataBaseDataSet.Customers' table. You can move, or remove it, as needed.
            this.customersTableAdapter.Fill(this.dataBaseDataSet.Customers);
            RefreshCustomersTable();
        }

        private void RefreshCustomersTable()
        {
            
            db.CustomersData();
            dataGridView1.DataSource = DB.ds.Tables[0];
            dataGridView1_CellContentClick(this, new DataGridViewCellEventArgs(0, 0));
        }
        private void Refresh_Click(object sender, EventArgs e)
        {
            RefreshCustomersTable();
        }
        private void Delete(object sender, EventArgs e)
        {
            int index = 0;
            if (dataGridView1.SelectedRows.Count > 0 && dataGridView1.RowCount > 0)
                index = dataGridView1.SelectedRows[0].Index;
            string strSql = "DELETE FROM Customers WHERE ClientID=" + dataGridView1.SelectedRows[0].Cells[0].Value.ToString() + "";
            db.Query(strSql);
            MessageBox.Show("Customer with ID " + dataGridView1.SelectedRows[0].Cells[0].Value.ToString() + " deleted");
            RefreshCustomersTable();
        }

        private void Update(object sender, EventArgs e)
        {
            int index = 0;
            if (dataGridView1.SelectedRows.Count > 0 && dataGridView1.RowCount > 0)
                index = dataGridView1.SelectedRows[0].Index;
            if (textBox1.Text != "" && textBox2.Text != "" &&
                textBox3.Text != "" &&
                textBox4.Text != "" &&
                textBox5.Text != "")
            {
                bool isSup = false;
                db.Query("Select * from Customers");
                for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
                {

                    if (dataGridView1.SelectedRows[0].Cells[0].Value.ToString() == DB.ds.Tables[0].Rows[i]["ClientID"].ToString())
                    {
                        isSup = true;
                        break;
                    }

                }
                if (isSup)
                {
                    List<string> statements = new List<string>();
                    statements.Add("UPDATE Customers new SET [ClientName]='" + textBox2.Text + "'" + " WHERE ClientID =" + dataGridView1.SelectedRows[0].Cells[0].Value.ToString());//פעולת העידכון
                    statements.Add("UPDATE Customers new SET [Age]=" + textBox3.Text + " WHERE ClientID =" + dataGridView1.SelectedRows[0].Cells[0].Value.ToString());//פעולת העידכון
                    statements.Add("UPDATE Customers new SET [ClientEmail]='" + textBox4.Text + "'" + " WHERE ClientID =" + dataGridView1.SelectedRows[0].Cells[0].Value.ToString());//פעולת העידכון
                    statements.Add("UPDATE Customers new SET [ClientAddress]='" + textBox5.Text + "'" + " WHERE ClientID =" + dataGridView1.SelectedRows[0].Cells[0].Value.ToString());//פעולת העידכון
                    statements.Add("UPDATE Customers new SET [ClientID]=" + textBox1.Text + " WHERE ClientID =" + dataGridView1.SelectedRows[0].Cells[0].Value.ToString());//פעולת העידכון
                    for (int i = 0; i < 5; i++)
                        db.Query(statements.ToArray()[i]);
                    MessageBox.Show("Client updated succesfully");
                    RefreshCustomersTable();
                }
                else
                    MessageBox.Show("Client with ID: " + textBox1.Text + " is not in the system");
                
            }
        }

        private void Back(object sender, EventArgs e)
        {
            this.sender.Show();
            this.Hide();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = e.RowIndex;
            if (row > -1)
            {
                dataGridView1.ClearSelection();
                dataGridView1.Rows[row].Selected = true;
                textBox1.Text = dataGridView1.Rows[row].Cells[0].Value.ToString();
                textBox2.Text = dataGridView1.Rows[row].Cells[1].Value.ToString();
                textBox3.Text = dataGridView1.Rows[row].Cells[2].Value.ToString();
                textBox4.Text = dataGridView1.Rows[row].Cells[3].Value.ToString();
                textBox5.Text = dataGridView1.Rows[row].Cells[4].Value.ToString();
            }
        }

    }
}
