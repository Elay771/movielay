﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class WorkersDataShowAndUpdate : Form
    {
        DB db;
        Form sender;
        string WorkerID;
        public WorkersDataShowAndUpdate(DB db, Form sender, string WorkerID)
        {
            this.sender = sender;
            this.WorkerID = WorkerID;
            this.db = db;
            InitializeComponent();
        }

        private void WorkersDataShow_Load(object sender, EventArgs e)
        {
            RefreshWorkersTable();            


        }
        private void RefreshWorkersTable()
        {
            db.WorkersData();
            dataGridView1.DataSource = DB.ds.Tables[0];
            dataGridView1_CellContentClick(this, new DataGridViewCellEventArgs(0, 0));


        }
        private void Refresh_Click(object sender, EventArgs e)
        {
            RefreshWorkersTable();
        }

        

        private void Update(object sender, EventArgs e)
        {
            int index = 0;
            if (dataGridView1.SelectedRows.Count > 0 && dataGridView1.RowCount > 0)
                index = dataGridView1.SelectedRows[0].Index;
            if (textBox1.Text!=""&& textBox2.Text != "" &&
                textBox6.Text != "" && textBox3.Text != "" &&
                textBox7.Text != "" && textBox4.Text != "" &&
                textBox8.Text != "" && textBox5.Text != "")
            {
            bool isSup = false;
            db.Query("Select * from Workers");
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {
                if (dataGridView1.SelectedRows[0].Cells[0].Value.ToString() == DB.ds.Tables[0].Rows[i]["WorkerID"].ToString())
                {
                    isSup = true;
                    break;
                }

            }
            if (isSup)
            {
                List<string> statements=new List<string>();
                statements.Add("UPDATE Workers new SET [WorkerName]='" + textBox2.Text + "' , [Age]=" + textBox3.Text + " , [JoinningDate]='" + textBox4.Text + "' , [WorkerPosition]='" + textBox5.Text + "' , [SalaryPerHour]=" + textBox6.Text + " , [Password]='" + textBox7.Text + "' , [Email]='" + textBox8.Text + "'" + " WHERE WorkerID =" + dataGridView1.SelectedRows[0].Cells[0].Value);//פעולת העידכון
                statements.Add("UPDATE Workers new SET [WorkerID]=" + textBox1.Text + " WHERE WorkerID =" +dataGridView1.SelectedRows[0].Cells[0].Value.ToString());//פעולת העידכון
                for (int i = 0; i < 2; i++)
                    db.Query(statements.ToArray()[i]);
                MessageBox.Show("Worker updated succesfully");
                RefreshWorkersTable();
            }
            else
                MessageBox.Show("Worker with ID: " + textBox1.Text + " is not in the system");
        }
    }
       
        private void DeleteButton(object sender, EventArgs e)
        {
            int index = 0;
            if (dataGridView1.SelectedRows.Count > 0 && dataGridView1.RowCount > 0)
                index = dataGridView1.SelectedRows[0].Index;
            string strSql = "DELETE FROM Workers WHERE WorkerID =" + dataGridView1.SelectedRows[0].Cells[0].Value + "";
            db.Query(strSql);
            MessageBox.Show("Worker with ID " + dataGridView1.SelectedRows[0].Cells[0].Value + " deleted");
            RefreshWorkersTable();
        }

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            this.sender.Show();
        }




        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int row= e.RowIndex;
            if (row > -1)
            {
                dataGridView1.ClearSelection();
                dataGridView1.Rows[row].Selected = true;
                textBox1.Text = dataGridView1.Rows[row].Cells[0].Value.ToString();
                textBox2.Text = dataGridView1.Rows[row].Cells[1].Value.ToString();
                textBox3.Text = dataGridView1.Rows[row].Cells[2].Value.ToString();
                textBox4.Text = dataGridView1.Rows[row].Cells[3].Value.ToString();
                textBox5.Text = dataGridView1.Rows[row].Cells[4].Value.ToString();
                textBox6.Text = dataGridView1.Rows[row].Cells[5].Value.ToString();
                textBox7.Text = dataGridView1.Rows[row].Cells[6].Value.ToString();
                textBox8.Text = dataGridView1.Rows[row].Cells[7].Value.ToString();
            }
        }

        
    }
}

