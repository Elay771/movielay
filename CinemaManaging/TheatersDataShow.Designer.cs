﻿namespace CinemaManaging
{
    partial class TheatersDataShow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label4 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.theaterIDDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numRowsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numColsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.theatersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataBaseDataSet = new CinemaManaging.DataBaseDataSet();
            this.theatersTableAdapter = new CinemaManaging.DataBaseDataSetTableAdapters.TheatersTableAdapter();
            this.Back = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.theatersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataBaseDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.label4.Location = new System.Drawing.Point(12, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(122, 31);
            this.label4.TabIndex = 50;
            this.label4.Text = "Theaters";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.theaterIDDataGridViewTextBoxColumn,
            this.numRowsDataGridViewTextBoxColumn,
            this.numColsDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.theatersBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(18, 59);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(430, 328);
            this.dataGridView1.TabIndex = 51;
            // 
            // theaterIDDataGridViewTextBoxColumn
            // 
            this.theaterIDDataGridViewTextBoxColumn.DataPropertyName = "TheaterID";
            this.theaterIDDataGridViewTextBoxColumn.HeaderText = "TheaterID";
            this.theaterIDDataGridViewTextBoxColumn.Name = "theaterIDDataGridViewTextBoxColumn";
            this.theaterIDDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // numRowsDataGridViewTextBoxColumn
            // 
            this.numRowsDataGridViewTextBoxColumn.DataPropertyName = "NumRows";
            this.numRowsDataGridViewTextBoxColumn.HeaderText = "NumRows";
            this.numRowsDataGridViewTextBoxColumn.Name = "numRowsDataGridViewTextBoxColumn";
            this.numRowsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // numColsDataGridViewTextBoxColumn
            // 
            this.numColsDataGridViewTextBoxColumn.DataPropertyName = "NumCols";
            this.numColsDataGridViewTextBoxColumn.HeaderText = "NumCols";
            this.numColsDataGridViewTextBoxColumn.Name = "numColsDataGridViewTextBoxColumn";
            this.numColsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // theatersBindingSource
            // 
            this.theatersBindingSource.DataMember = "Theaters";
            this.theatersBindingSource.DataSource = this.dataBaseDataSet;
            // 
            // dataBaseDataSet
            // 
            this.dataBaseDataSet.DataSetName = "DataBaseDataSet";
            this.dataBaseDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // theatersTableAdapter
            // 
            this.theatersTableAdapter.ClearBeforeFill = true;
            // 
            // Back
            // 
            this.Back.Location = new System.Drawing.Point(740, 415);
            this.Back.Name = "Back";
            this.Back.Size = new System.Drawing.Size(48, 23);
            this.Back.TabIndex = 52;
            this.Back.Text = "Back";
            this.Back.UseVisualStyleBackColor = true;
            this.Back.Click += new System.EventHandler(this.Back_Click);
            // 
            // TheatersDataShow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Back);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label4);
            this.Name = "TheatersDataShow";
            this.Load += new System.EventHandler(this.TheatersDataShow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.theatersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataBaseDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dataGridView1;
        private DataBaseDataSet dataBaseDataSet;
        private System.Windows.Forms.BindingSource theatersBindingSource;
        private DataBaseDataSetTableAdapters.TheatersTableAdapter theatersTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn theaterIDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numRowsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn numColsDataGridViewTextBoxColumn;
        private System.Windows.Forms.Button Back;
    }
}