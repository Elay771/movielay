﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class MostActiveClient : Form
    {
        DB db;
        DateTime from, to;
        Form sender;
        string WorkerID;
        public MostActiveClient(DB db, DateTime from, DateTime to, Form sender, string WorkerID)
        {
            this.sender = sender;
            this.WorkerID = WorkerID;
            this.from = from;
            this.to = to;
            this.db = db;
            InitializeComponent();
        }

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            this.sender.Show();
        }
        private void MostActiveClient_Load(object sender, EventArgs e)
        {
            int max = -1;
            string maxID = "";
            db.Query("Select * from Customers");
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {
                string currWorkerID = DB.ds.Tables[0].Rows[i]["ClientID"].ToString();
                db.Query("Select Count(*) from TheaterMovies RIGHT JOIN MovieOrders  ON TheaterMovies.TheaterMovieID = MovieOrders.TheaterMovieID  where MovieOrders.ClientID = " + currWorkerID + " and TheaterMovies.StartTime between #" + from.ToString() + "# and #" + to.ToString() + "#;");
                int curr = Int32.Parse(DB.ds.Tables[0].Rows[0].ItemArray[0].ToString());
                if (max < curr)
                {
                    max = curr;
                    maxID = currWorkerID;
                }
                db.Query("Select * from Customers");
            }
            if (maxID !="")
            {
                db.Query("Select * from Customers where ClientID=" + maxID + ";");
                label7.Text = DB.ds.Tables[0].Rows[0]["ClientID"].ToString();
                label8.Text = DB.ds.Tables[0].Rows[0]["ClientName"].ToString();
                label9.Text = DB.ds.Tables[0].Rows[0]["Age"].ToString();
                label10.Text = DB.ds.Tables[0].Rows[0]["ClientEmail"].ToString();
                label11.Text = DB.ds.Tables[0].Rows[0]["ClientAddress"].ToString();
                label12.Hide();

            }
            else
            {
                foreach (Label l in this.Controls.OfType<Label>().ToList())
                    l.Hide();
                label12.Show();
            }

        }
    }
}
