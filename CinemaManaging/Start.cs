﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class Start : Form
    {
        DB db;
        public string ID;
        public Start()
        {
            this.db = new DB();
            InitializeComponent();
        }
        public Start(DB db)
        {
            this.db = db;
            InitializeComponent();
        }
        private void Login_Click(object sender, EventArgs e)
        {
            this.Hide();
            var login = new Login(db);
            login.Closed += (s, args) => this.Close();
            login.Show();
        }

        private void NewWorker_Click(object sender, EventArgs e)
        {
            this.Hide();
            var newWorker= new NewWorker(db);
            newWorker.Closed += (s, args) => this.Close();
            newWorker.Show();
        }

        private void Start_Load(object sender, EventArgs e)
        {

        }
    }
}
