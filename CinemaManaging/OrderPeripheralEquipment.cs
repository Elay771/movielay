﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class OrderPeripheralEquipment : Form
    {
        DB db; 
        string WorkerID; 
        Form prev;
        Dictionary<string, int> itemNameAndAmount;
        public OrderPeripheralEquipment(DB db, string WorkerID, Form prev)// Previous form workerid and datebase
        {
            itemNameAndAmount = new Dictionary<string, int>();
            this.db = db;
            this.WorkerID = WorkerID;
            this.prev = prev;
            InitializeComponent();
        }

        


        private void AddToOrder(object sender, EventArgs e)
        {
            try
            {
                string itemName = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
                
                int amount = Int32.Parse(textBox1.Text);
                textBox1.Text = "";
                if (amount <= 0)
                    MessageBox.Show("Error! Invalid Amount");
                else if (!itemNameAndAmount.ContainsKey(itemName))
                    itemNameAndAmount.Add(itemName, amount);
                else
                    itemNameAndAmount[itemName] += amount;
            }
            catch
            {
                MessageBox.Show("Error! Invalid Amount");
            }
        }

       

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            prev.Show();
        }

        private void sendOrderToEmail(object sender, EventArgs e)
        {
            db.Query("Select * from Workers where WorkerID=" + WorkerID);
            string workerName = DB.ds.Tables[0].Rows[0]["WorkerName"].ToString();
            string content = "Hi, Supplier\nMoviElay Cinema Would Like to Order:\n";
            foreach (string item in itemNameAndAmount.Keys)
                content += itemNameAndAmount[item] + " " + item + "\n";
            content += "Thank You, " + workerName + " from MoviElay Cinema";
            this.Hide();
            var next = new SelectSupplierAndSendEmail(WorkerID, db, this, "Peripheral Equipment Order From MoviElay", content,prev);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void OrderPeripheralEquipment_Load(object sender, EventArgs e)
        {
            db.Query("Select * from Items where Type='Peripheral'");
            dataGridView1.DataSource = DB.ds.Tables[0];
            dataGridView1.Rows[0].Selected = true;
        }



        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = e.RowIndex;
            if (row > -1)
            {
                dataGridView1.ClearSelection();
                dataGridView1.Rows[row].Selected = true;
            }
        }
    }

}
