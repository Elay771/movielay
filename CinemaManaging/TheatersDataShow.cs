﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class TheatersDataShow : Form
    {
        DB db;
        Form sender;
        string WorkerID;
        public TheatersDataShow(DB db, Form sender, string WorkerID)
        {
            this.sender = sender;
            this.WorkerID = WorkerID;
            this.db = db;
            InitializeComponent();
        }

        private void TheatersDataShow_Load(object sender, EventArgs e)
        {
            RefreshWorkersTable();
        }
        private void RefreshWorkersTable()
        {
            db.Query("Select * from Theaters");
            dataGridView1.DataSource = DB.ds.Tables[0];
            dataGridView1.Rows[0].Selected = true;

        }


        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.sender.Show();
        }

        private void Back_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.sender.Show();
        }
    }
}
