﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class MostOrderedTheater : Form
    {
        DB db;
        DateTime from, to;
        Form sender;
        string WorkerID;
        public MostOrderedTheater(DB db, DateTime from, DateTime to, Form sender, string WorkerID)
        {
            this.sender = sender;
            this.WorkerID = WorkerID;
            this.from = from;
            this.to = to;
            this.db = db;
            InitializeComponent();
        }

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            this.sender.Show();
        }

        private void MostOrderedTheater_Load(object sender, EventArgs e)
        {
            int max = -1;
            int maxTheaterID = -1;
            db.Query("Select * from Theaters");
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {
                int currTheaterID = Int32.Parse(DB.ds.Tables[0].Rows[i]["TheaterID"].ToString());
                db.Query("Select Count(*) from TheaterMovies RIGHT JOIN MovieOrders  ON TheaterMovies.TheaterMovieID = MovieOrders.TheaterMovieID  where TheaterMovies.TheaterID = " + currTheaterID + " and TheaterMovies.StartTime between #" + from.ToString() + "# and #" + to.ToString() + "#;");
                int curr = Int32.Parse(DB.ds.Tables[0].Rows[0].ItemArray[0].ToString());
                if (max < curr)
                {
                    max = curr;
                    maxTheaterID = currTheaterID;
                }
                db.Query("Select * from Theaters");
            }
            if (maxTheaterID >= 0)
            {
                db.Query("Select * from Theaters where TheaterID=" + maxTheaterID + ";");
                label5.Hide();
                label7.Text = DB.ds.Tables[0].Rows[0]["TheaterID"].ToString();
                label8.Text = DB.ds.Tables[0].Rows[0]["NumRows"].ToString();
                label9.Text = DB.ds.Tables[0].Rows[0]["NumCols"].ToString();
            }
            else
            {
                foreach (Label l in this.Controls.OfType<Label>().ToList())
                    l.Hide();
                label5.Show();
            }
        }
    }
}
