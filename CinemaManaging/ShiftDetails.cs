﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class ShiftDetails : Form
    {
        DB db;
        Form sender;
        string workerID;
        DateTime from;
        DateTime to;
        public ShiftDetails(DB db, Form sender, string workerID, DateTime from, DateTime to)
        {
            this.from = from;
            this.to = to;
            this.workerID = workerID;
            this.db = db;
            this.sender = sender;
            InitializeComponent();
            
        }
        private void refresh()
        {
            DateTime startTime = getStartTime();
            db.Query("Select Workers.WorkerID, Workers.WorkerName from Shifts RIGHT JOIN [Workers] ON Shifts.WorkerID=[Workers].[WorkerID] where StartTime=#" + startTime + "#");
            dataGridView1.DataSource = DB.ds.Tables[0];
            try { dataGridView1.Rows[0].Selected = true; }
            catch { }
        }
        private void ShiftDetails_Load(object sender, EventArgs e)
        {
            this.listBox1.SelectedIndex = 0;
            this.listBox2.SelectedIndex = 0;
            refresh();
            
        }

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            this.sender.Show();
        }

        private void Insert(object sender, EventArgs e)
        {
            DateTime startTime = getStartTime();
            
            db.Query("Select Count(*) from Shifts where StartTime=#" + startTime + "# AND WorkerID=" + textBox1.Text + ";");
            if (textBox1.Text != "" && DB.ds.Tables[0].Rows[0][0].ToString() == "0")
            {
                db.Query("INSERT INTO Shifts([WorkerID],[StartTime]) VALUES(" + textBox1.Text + ", '" + startTime + "')");
                MessageBox.Show("Worker with ID " + textBox1.Text + " added");
                refresh();
            }
        }

        private void Delete(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                DateTime startTime = getStartTime();
                string strSql = "DELETE FROM Shifts WHERE StartTime=#" + startTime + "# WorkerID =" + dataGridView1.SelectedRows[0].Cells[0].Value + "";
                db.Query(strSql);
                MessageBox.Show("Worker with ID " + dataGridView1.SelectedRows[0].Cells[0].Value + " deleted");
            }
        }
        private DateTime getStartTime()
        {
            int day = listBox1.SelectedIndex;
            int type = listBox2.SelectedIndex;
            DateTime startTime = new DateTime(from.Year, from.Month, from.Day, from.Hour, from.Minute, from.Second);
            startTime=startTime.AddDays(day);
            startTime=startTime.AddHours(10+(type * 6));
            return startTime;
        }
        private void Workers(object sender, EventArgs e)
        {
            this.Hide();
            var next = new WorkersDataShow(db, this, workerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void listIndexChanged(object sender, EventArgs e)
        {
            refresh();
        }

   
    }
}
