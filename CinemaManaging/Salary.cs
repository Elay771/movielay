﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CinemaManaging
{
    class Salary
    {
        public static string getSalary(string access,int age)
        {
            int salary = 0;
            if (access.CompareTo("Manager") == 0)
                salary += 40;
            else if (access.Contains("Shift Manager"))
                salary += 10;
            else if (access.CompareTo("Assistant Manager") == 0)
                salary += 20;
            else
                salary += 5;

            if (age >= 18)
                salary += 28;
            else if (age == 17)
                salary += 25;
            else if (age >= 16)
                salary += 22;
            else
                salary += 20;

            return salary.ToString();
        }
    }
}
