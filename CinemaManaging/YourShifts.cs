﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class YourShifts : Form
    {
        DB db;
        string WorkerID;
        Form prev;
        public YourShifts(DB db, string WorkerID, Form prev)// Previous form workerid and datebase
        {
            this.db = db;
            this.WorkerID = WorkerID;
            this.prev = prev;
            InitializeComponent();
        }
        private void YourShifts_Load(object sender, EventArgs e)
        {
            db.Query("Select * from Shifts WHERE WorkerID =" + WorkerID);
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {   
                listBox1.Items.Add(DB.ds.Tables[0].Rows[i]["StartTime"].ToString()); // Add Shift Start Times to List
                listBox2.Items.Add(DB.ds.Tables[0].Rows[i]["ShiftID"].ToString()); // Add Shift ID to List

            }
        }

        private void Back_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.prev.Show();
        }

        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = ((ListBox)sender).SelectedIndex;
            Program.changeSelectedIndex(index, this.Controls.OfType<ListBox>().ToList());// change selected index in all lists
        }

    }
}
