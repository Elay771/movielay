﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class MovieOrdersOfClientShow : Form
    {
        DB db;
        int clientID;
        Form sender;
        public MovieOrdersOfClientShow(DB db,Form sender, int clientID)
        {
            this.sender = sender;
            this.clientID = clientID;
            this.db = db;
            InitializeComponent();
        }

        private void MovieOrdersShow_Load(object sender, EventArgs e)
        {
            RefreshMovieOrdersTable();
        }
        private void RefreshMovieOrdersTable()
        {
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            listBox4.Items.Clear();
            listBox5.Items.Clear();
            listBox7.Items.Clear();

            db.Query("Select * from MovieOrders where ClientID="+ clientID + ";");
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {
                listBox2.Items.Add(DB.ds.Tables[0].Rows[i]["TheaterMovieID"].ToString());
                listBox3.Items.Add(DB.ds.Tables[0].Rows[i]["Row"].ToString());
                listBox4.Items.Add(DB.ds.Tables[0].Rows[i]["Col"].ToString());
                listBox5.Items.Add(DB.ds.Tables[0].Rows[i]["WorkerID"].ToString());
                db.Query("Select * from TheaterMovies where TheaterMovieID=" + DB.ds.Tables[0].Rows[i]["TheaterMovieID"].ToString() + ";");
                listBox7.Items.Add(DB.ds.Tables[0].Rows[i]["StartTime"].ToString());
            }

        }

        private void Refresh_Click(object sender, EventArgs e)
        {
            RefreshMovieOrdersTable();
        }

        private void Back(object sender, EventArgs e)
        {
            this.sender.Show();
            this.Hide();
        }

        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = ((ListBox)sender).SelectedIndex;
            Program.changeSelectedIndex(index, this.Controls.OfType<ListBox>().ToList());// change selected index in all lists
        }


    }
}
