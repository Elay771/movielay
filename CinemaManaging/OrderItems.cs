﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class OrderItems : Form
    {
        DB db;
        string WorkerID;
        Form prev;
        public OrderItems(DB db, string WorkerID, Form prev)// Previous form workerid and datebase
        {
            this.db = db;
            this.WorkerID = WorkerID;
            this.prev = prev;
            InitializeComponent();
        }

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            this.prev.Show();
        }

        private void OrderUniformClick(object sender, EventArgs e)
        {
            this.Hide();
            var next = new OrderUniform(new Dictionary<string, int>(),db, WorkerID,this);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void OrderPeriferialClick(object sender, EventArgs e)
        {
            this.Hide();
            var next = new OrderPeripheralEquipment(db, WorkerID, this);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void OrderSupplyClick(object sender, EventArgs e)
        {
            this.Hide();
            var next = new OrderSupply(db, WorkerID, this);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void OrderItems_Load(object sender, EventArgs e)
        {

        }
    }
}
