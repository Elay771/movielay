﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;

namespace CinemaManaging
{
    public partial class Food_Court_Order : Form
    {
        DB db;
        string WorkerID;
        Form prev;
        Dictionary<string, int> ItemAmounts;
        int numPages;
        int currPage;
        public Food_Court_Order(DB db, string WorkerID, Form prev, Dictionary<string, int> ItemAmounts=null)// Previous form workerid and datebase
        {
            this.prev = prev;
            this.db = db;
            this.WorkerID = WorkerID;
            this.prev = prev;
            db.Query("Select Count(*) from Items Where Type = 'Court'");
            numPages = (Int32.Parse(DB.ds.Tables[0].Rows[0].ItemArray[0].ToString()) % 6) + 1;
            currPage = 1;
            InitializeComponent();
            loadCourtFood();
            PrevButton.Enabled = false;
        }

        public void loadCourtFood()
        {
            int numItemsInPage = 6;
            db.Query("Select * from Items Where Type = 'Court'");
            int numItems = DB.ds.Tables[0].Rows.Count;
            if (numPages == currPage&&numItems%6!=0)
                numItemsInPage = numItems%6;
            foreach(GroupBox g in groupBox0.Controls.OfType<GroupBox>().ToList())
                g.Hide();
            for (int i = 0; i < numItemsInPage; i++)
            {
                GroupBox currBox =groupBox0.Controls.OfType<GroupBox>().ToList()[i];
                currBox.Show();
                currBox.Text = DB.ds.Tables[0].Rows[i+((currPage-1)*6)]["ItemName"].ToString();
                try { currBox.Controls.OfType<PictureBox>().ToList()[0].Image = Image.FromFile(DB.ds.Tables[0].Rows[i + ((currPage - 1) * 6)]["ImagePath"].ToString()); }
                catch { }
                currBox.Controls.OfType<TextBox>().ToList()[0].Text = "";
            }
            if(numPages==1)
            {
                PrevButton.Enabled = false;
                NextButton.Enabled = false;
            }
        }

        private void NextClick(object sender, EventArgs e)
        {
            currPage += 1;
            PrevAndNextButtons();
            loadCourtFood(); 
        }
        private void PrevAndNextButtons()
        {
            NextButton.Enabled = true;
            PrevButton.Enabled = true;
            if (currPage == numPages)
                NextButton.Enabled = false;
            if (currPage == 1)
                PrevButton.Enabled = false;
        }
        private void PrevClick(object sender, EventArgs e)
        {
            currPage -= 1;
            PrevAndNextButtons();
            loadCourtFood();
        }

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            this.prev.Show();
        }

        private void AddToCart(object sender, EventArgs e)
        {
            try
            {
                string addButtonNumber = ((Button)sender).Name.Last().ToString();
                GroupBox curr = (GroupBox)(groupBox0.Controls["groupBox" + addButtonNumber]);
                int amount = Int32.Parse(((TextBox)(curr.Controls["textBox" + addButtonNumber])).Text);
                db.Query("Select ItemID from Items where ItemName='" + curr.Text+"'");
                string itemID = DB.ds.Tables[0].Rows[0].ItemArray[0].ToString();
                int index = listBox1.Items.IndexOf(itemID);
                if (ItemAmounts.ContainsKey(itemID) && amount > 0)
                {
                    if (Int32.Parse(listBox4.Items[index].ToString()) >= (amount + ItemAmounts[itemID]))
                        ItemAmounts[itemID] += amount;
                    else
                        MessageBox.Show("Invalid Amount!");
                }
                else
                {
                    MessageBox.Show("Invalid Amount!");
                }
                curr.Controls["textBox" + addButtonNumber].Text = "";
            }
            catch { MessageBox.Show("Please Enter a Number!"); }
        }


        private void Food_Court_Order_Load(object sender, EventArgs e)
        {
            try
            { int testNul =ItemAmounts.Count; }
            catch { ItemAmounts = new Dictionary<string, int>(); }
            db.Query("SELECT * FROM Items");
            foreach (DataRow i in DB.ds.Tables[0].Rows)
            {
                if (i["Type"].ToString() == "Court" && i["itemID"].ToString() != "0")
                {
                    listBox4.Items.Add(i["ItemQuantity"].ToString());
                    listBox1.Items.Add(i["ItemID"].ToString());
                    ItemAmounts.Add(i["ItemID"].ToString(), 0);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new Cart(db, WorkerID, this, ItemAmounts,this.prev);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }
    }
}
