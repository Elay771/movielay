﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class NewClient : Form
    {
        DB db;
        Form sender;
        string WorkerID;
        public NewClient(DB db, Form sender, string WorkerID)
        {
            this.sender = sender;
            this.WorkerID = WorkerID;
            this.db = db;
            InitializeComponent();
        }

        private void AddNewClient_Click(object sender, EventArgs e)
        {
            Error.Hide();
            if (Int32.TryParse(IDBox.Text, out int i) && i > 0 &&
                Int32.TryParse(AgeBox.Text, out int j) && i > 0 &&
                Regex.IsMatch(NameBox.Text, @"^[a-zA-Z]+$")
                 && EmailBox.Text.Contains('@') && EmailBox.Text.Contains('.')
                && !db.doesClientExist(IDBox.Text))
            {
                
                db.addClientToDB(IDBox.Text, NameBox.Text,Age.Text,EmailBox.Text, AddressBox.Text);
            }
            else
            {
                Error.Show();
            }
        }

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            this.sender.Show();
        }

        private void NewClient_Load(object sender, EventArgs e)
        {

        }
    }
}
