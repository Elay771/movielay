﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class ManageShifts : Form
    {
        DB db;
        Form sender;
        string workerID;
        public ManageShifts(DB db,Form sender,string workerID)
        {
            this.workerID = workerID;
            this.db = db;
            this.sender = sender;
            InitializeComponent();
        }

        private void ShiftsOfTheWeek_Load(object sender, EventArgs e)
        {
            
        }

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            this.sender.Show();
        }

        private void GetShift(object sender, EventArgs e)
        {
            int sunDiff = (7 + (dateTimePicker1.Value.DayOfWeek - DayOfWeek.Sunday)) % 7;
            int satDiff = ((7 + (dateTimePicker1.Value.DayOfWeek - DayOfWeek.Saturday)) % 7);
            DateTime sunday= new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day,0,0,0).AddDays(-1 * sunDiff).Date;
            DateTime saturday= new DateTime(dateTimePicker1.Value.Year, dateTimePicker1.Value.Month, dateTimePicker1.Value.Day, 23,59,59).AddDays((-1 * satDiff)+7).Date;
            this.Hide();
            var next = new ShiftDetails(db, this, workerID, sunday,saturday);
            next.Closed += (s, args) => this.Close();
            next.Show();

        }
    }
}
