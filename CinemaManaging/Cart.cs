﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class Cart : Form
    {
        DB db;
        string WorkerID;
        Form prev;

        Dictionary<string, int> ItemAmounts;
        Dictionary<string, int> prevItemAmounts;
        Form prevOfOrderScreen;
        public Cart(DB db, string WorkerID, Form prev,Dictionary<string,int> ItemAmounts, Form prevOfOrderScreen)// Previous form workerid and datebase
        {
            this.prevOfOrderScreen = prevOfOrderScreen;
            prevItemAmounts = new Dictionary<string, int>();
            this.ItemAmounts = ItemAmounts;
            this.db = db;
            this.WorkerID = WorkerID;
            this.prev = prev;
            InitializeComponent();
        }

        private void Cart_Load(object sender, EventArgs e)
        {
            int sum = 0;
            foreach (string itemID in ItemAmounts.Keys)
            {
                if (ItemAmounts[itemID] > 0)
                {
                    db.Query("SELECT * FROM Items where ItemID="+itemID);
                    listBox1.Items.Add(DB.ds.Tables[0].Rows[0]["itemName"].ToString());
                    listBox2.Items.Add(ItemAmounts[itemID]);
                    foreach (DataRow i in DB.ds.Tables[0].Rows)
                    {
                        if (i["ItemID"].ToString() == itemID)
                        {
                            prevItemAmounts.Add(itemID, Int32.Parse(i["ItemPrice"].ToString()));
                            int priceTimesAmount = Int32.Parse(i["ItemPrice"].ToString()) * ItemAmounts[itemID];
                            sum += priceTimesAmount;
                            listBox3.Items.Add(priceTimesAmount);
                        }
                    }
                }
            }
            TotalPrice.Text = sum.ToString();
        }

        private void NewClient_Click(object sender, EventArgs e)
        {

        }

        private void Done_Click(object sender, EventArgs e)
        {
            foreach (string itemID in ItemAmounts.Keys)
            {
                db.Query("UPDATE Items new SET [ItemQuantity]='" + (prevItemAmounts[itemID]+ItemAmounts[itemID]).ToString() + "'" + " WHERE ClientID =" + itemID);//פעולת העידכון
            }
            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();
        }

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            var next = new Food_Court_Order(db, WorkerID, prevOfOrderScreen, ItemAmounts);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void listBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            int index = ((ListBox)sender).SelectedIndex;
            Program.changeSelectedIndex(index, this.Controls.OfType<ListBox>().ToList());// change selected index in all lists
        }

    }
}
