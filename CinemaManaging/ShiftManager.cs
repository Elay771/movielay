﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class ShiftManager : Form
    {
        DB db;
        string WorkerID;
        public ShiftManager(DB db, string WorkerID)
        {
            this.WorkerID = WorkerID;
            this.db = db;
            InitializeComponent();
        }

        private void ShiftManager_Load(object sender, EventArgs e)
        {

        }

        private void LogOut_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new Start(db);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void YourData_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new WorkerData(db, this, WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }


        private void YourShifts_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new YourShifts(db, WorkerID, this);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void ShiftsOfTheWeek_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            var next = new ManageShifts(db, this, this.WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void YourSalary_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new CalculateSalary(db, WorkerID, this);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }
    }
}
