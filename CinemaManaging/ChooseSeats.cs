﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class ChooseSeats : Form
    {
        DB db;
        List<List<PictureBox>> seats;
        string selectedTheaterMovie;
        string path;
        string WorkerID;
        Form sender;
        public ChooseSeats(DB db, string selected,Form sender, string WorkerID)
        {
            this.sender = sender;
            this.WorkerID = WorkerID;
            path = AppDomain.CurrentDomain.BaseDirectory;
            path = path.Substring(0, path.Length - 10);
            seats = new List<List<PictureBox>>();
            this.db = db;
            this.selectedTheaterMovie = selected;
            InitializeComponent();
        }

        private void ChooseSeats_Load(object sender, EventArgs e)
        {
            Form back = sender as Form;
            int TheaterID,numRows,numCols;
            
            db.Query("Select * from TheaterMovies where TheaterMovieID="+selectedTheaterMovie+"; ");
            TheaterID = Int32.Parse(DB.ds.Tables[0].Rows[0]["TheaterID"].ToString());
            db.Query("Select * from Theaters where TheaterID=" + TheaterID + ";");
            numRows = Int32.Parse(DB.ds.Tables[0].Rows[0]["NumRows"].ToString());
            numCols = Int32.Parse(DB.ds.Tables[0].Rows[0]["NumCols"].ToString());
            for (int i = 0; i < numRows; i++)
            {
                seats.Add(new List<PictureBox>());
                for (int j = 0; j < numCols; j++)
                {
                    PictureBox pic = new PictureBox();
                    pic.SizeMode = PictureBoxSizeMode.AutoSize;
                    pic.Location = new Point(i * 30, j * 50);
                    pic.Image = Image.FromFile(path + "Seat.png");
                    pic.ImageLocation = path + "Seat.png";
                    seats[i].Add(pic);
                    this.Controls.Add(seats[i][j]);
                    seats[i][j].Click += new EventHandler(seatClick);
                }
            }
            db.Query("select * from MovieOrders where TheaterMovieID="+selectedTheaterMovie+";");
            
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {
                int row=(Int32.Parse(DB.ds.Tables[0].Rows[i]["Row"].ToString()));
                int col=(Int32.Parse(DB.ds.Tables[0].Rows[i]["Col"].ToString()));
                seats[col-1][row-1].Image = Image.FromFile(path+ "TakenSeat.png");
                seats[col-1][row-1].ImageLocation = path+ "TakenSeat.png";
            }
            
            
            
        }
        private void seatClick(object sender, EventArgs e)
        {
            PictureBox seat = sender as PictureBox;
            if (seat != null)
            {
                if (seat.ImageLocation.Contains("ChosenSeat.png"))
                {
                    seat.Image = Image.FromFile(path + "Seat.png");
                    seat.ImageLocation = path + "Seat.png";
                }
                else
                {
                    seat.Image = Image.FromFile(path + "ChosenSeat.png");
                    seat.ImageLocation = path + "ChosenSeat.png";
                }
            }

        }

        private void Done_Click(object sender, EventArgs e)
        {
            bool didChoose=false;
            if (textBox1.Text != "") 
            {
                for (int i = 0; i < seats.Count(); i++)
                {
                    for (int j = 0; j < seats[i].Count(); j++)
                    {
                        if (seats[i][j].ImageLocation.Equals(path + "ChosenSeat.png"))
                        {
                            didChoose = true;
                            db.Query("INSERT INTO MovieOrders ([TheaterMovieID],[Row],[Col],[WorkerID],[ClientID]) VALUES(" + selectedTheaterMovie + "," + (j+1).ToString() + "," + (i + 1).ToString() + "," + WorkerID + "," + textBox1.Text + ");");
                            seats[i][j].Image = Image.FromFile(path + "TakenSeat.png");
                            seats[i][j].ImageLocation = path + "TakenSeat.png";
                        }
                    }
                }
            }
            if(didChoose)
                MessageBox.Show("Thanks For Buying Tickets");
            else
                MessageBox.Show("Please Choose at Least One Sit");
        }

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            this.sender.Show();
        }

        private void NewClient_Click(object sender, EventArgs e)
        {
            this.Hide();
            var next = new NewClient(db, this,WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }
    }
}
