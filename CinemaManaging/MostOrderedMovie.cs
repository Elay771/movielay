﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class MostOrderedMovie : Form
    {
        DB db;
        DateTime from, to;
        Form sender;
        string WorkerID;
        public MostOrderedMovie(DB db, DateTime from, DateTime to, Form sender, string WorkerID)
        {
            this.sender = sender;
            this.WorkerID = WorkerID;
            this.from = from;
            this.to = to;
            this.db = db;
            InitializeComponent();
        }

        private void MostOrderedMovie_Load(object sender, EventArgs e)
        {
            int max = -1;
            int maxMovieID = -1;
            db.Query("Select * FROM TheaterMovies where StartTime between #"+from.ToString()+"# and #"+to.ToString()+"#");
            for (int i = 0; i < DB.ds.Tables[0].Rows.Count; i++)
            {
                int id = Int32.Parse(DB.ds.Tables[0].Rows[i]["TheaterMovieID"].ToString());
                db.Query("Select Count(*) from MovieOrders where TheaterMovieID=" + id);
                int curr = Int32.Parse(DB.ds.Tables[0].Rows[0].ItemArray[0].ToString());
                if ( max < curr)
                {
                    max = curr;
                    maxMovieID = id;
                }
                db.Query("Select * FROM TheaterMovies where StartTime between #" + from.ToString() + "# and #" + to.ToString() + "#");
            }
            if (maxMovieID >= 0)
            {
                db.Query("Select * from Movies where MovieID=" + maxMovieID + ";");
                label12.Hide();
                label7.Text = DB.ds.Tables[0].Rows[0]["MovieID"].ToString();
                label8.Text = DB.ds.Tables[0].Rows[0]["MovieName"].ToString();
                label9.Text = DB.ds.Tables[0].Rows[0]["Genre"].ToString();
                label10.Text = DB.ds.Tables[0].Rows[0]["AgeLimit"].ToString();
                label11.Text = DB.ds.Tables[0].Rows[0]["RunningTimeInMinutes"].ToString();
            }
            else
            {
                foreach (Label l in this.Controls.OfType<Label>().ToList())
                    l.Hide();
                label12.Show();
            }
        }

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            this.sender.Show();
        }

    }
}
