﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class MovieTicketOrder : Form
    {
        DB db;
        Form sender;
        string WorkerID;
        public MovieTicketOrder(DB db,Form sender, string WorkerID)
        {
            this.sender = sender;
            this.WorkerID = WorkerID;
            this.db = db;
            InitializeComponent();
        }
     
        private void MovieTicketOrder_Load(object sender, EventArgs e)
        {
            RefreshMovieTable();
        }

        private void RefreshMovieTable()
        {
            db.Query("Select * from Movies");
            dataGridView1.DataSource = DB.ds.Tables[0];
            dataGridView1_CellContentClick(this, new DataGridViewCellEventArgs(0, 0));
        }

        private void Next(object sender, EventArgs e)
        {
            db.Query("Select * from Movies");
            int index = 0;
            if (dataGridView1.SelectedRows.Count>0 && dataGridView1.RowCount > 0)
                index = dataGridView1.SelectedRows[0].Index;
            this.Hide();
            var next = new ChooseMovieDate(db, DB.ds.Tables[0].Rows[index]["MovieID"].ToString(),this,WorkerID);
            next.Closed += (s, args) => this.Close();
            next.Show();
        }

        private void Back(object sender, EventArgs e)
        {
            this.Hide();
            this.sender.Show();
        }

        
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int row = e.RowIndex;
            dataGridView1.ClearSelection();
            dataGridView1.Rows[row].Selected = true;

        }

    }
}
