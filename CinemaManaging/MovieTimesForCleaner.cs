﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CinemaManaging
{
    public partial class MovieTimesForCleaner : Form
    {
        DB db;
        Form sender;
        string WorkerID;
        DateTime startTime;
        public MovieTimesForCleaner(DB db, Form sender, string WorkerID,DateTime startTime)
        {
            this.startTime = startTime;
            this.sender = sender;
            this.WorkerID = WorkerID;
            this.db = db;
            InitializeComponent();
        }

        private void MovieTimesForCleaner_Load(object sender, EventArgs e)
        {
            db.Query("SELECT * from TheaterMovies RIGHT JOIN [Movies] ON TheaterMovies.MovieID=[Movies].MovieID where StartTime between #" + startTime.ToString() + "# and #" + startTime.AddDays(1).AddMilliseconds(-1).ToString() + "#;");
            dataGridView1.DataSource = DB.ds.Tables[0];
            dataGridView1.Rows[0].Selected = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.sender.Show();
            this.Hide();
        }
    }
    
    
    

}
